package com.example.countdown;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.util.Log;

public class DateCount {

	
	DateCount(){
	}
	
	public int main2(int y, int m,int d) {
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
	    Date dateTo = null;
	    Date dateFrom = null;
	    Calendar cal1 = Calendar.getInstance();
		String year = String.valueOf(cal1.get(Calendar.YEAR));
		String month = String.valueOf(cal1.get(Calendar.MONTH)+1);
		String day = String.valueOf(cal1.get(Calendar.DATE));
	    
	    try {
	        dateFrom = sdf.parse(year+"/"+month+"/"+day);
	        dateTo = sdf.parse(y+"/"+m+"/"+d);
	    } catch (ParseException e) {
	        e.printStackTrace();
	    }
	 
	    long dateTimeTo = dateTo.getTime();
	    long dateTimeFrom = dateFrom.getTime();
	  
	    long dayDiff = ( dateTimeTo - dateTimeFrom  ) / (1000 * 60 * 60 * 24 );
		return (int)dayDiff;
	}
}
