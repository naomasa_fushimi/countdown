package com.example.countdown;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import android.content.Context;
import android.util.Log;

public class EventDelete {

	private static final String FileName = "event_save_file.txt";
	
	Context context;
	public EventDelete(Context c) {
		context = c;
	}

	public void delete(int position) {
		EventRead er = new EventRead(context);
		int i = er.readall().size();
//		Log.d("deletedata",String.valueOf(er.readall().size())+String.valueOf(position));
		try {
			context.deleteFile(FileName);
			FileOutputStream fileo = context.openFileOutput(FileName, Context.MODE_APPEND);
			OutputStreamWriter osw = new OutputStreamWriter(fileo);
			BufferedWriter bw = new BufferedWriter(osw);
			for(int j=0;j<i;j++){
				if(j != position){
					String line = String.valueOf(er.readall().get(j));
//					Log.d("newline",line);
					bw.write(line+"\r\n");
				}
			}
			bw.close();
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
	}
}
