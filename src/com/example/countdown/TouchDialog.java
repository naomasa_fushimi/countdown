package com.example.countdown;

import java.util.ArrayList;





import android.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class TouchDialog extends DialogFragment{
	
	ArrayList<String> list = new ArrayList<String>();
	ArrayList<String> list2 = new ArrayList<String>();
	static Context context;
	public static int position;
	public void listview(ArrayList<String> l,Context c){
		list = l;
		context = c;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState){
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		final MainActivity callback = (MainActivity)getActivity();
		ArrayAdapter<String> array = new ArrayAdapter<String>(context,R.layout.simple_list_item_1,list);
		ListView lv = new ListView(context);
		lv.setAdapter(array);
		lv.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(AdapterView<?> item, View view,
					final int p, long id) {
				position=p;
				AlertDialog.Builder builder2 = new AlertDialog.Builder(getActivity());
				builder2.setPositiveButton("表示", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Log.d("p",String.valueOf(p));
						
						callback.counterView(position);
					}
				});
				builder2.setNegativeButton("削除", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						EventDelete ed = new EventDelete(context);
						ed.delete(position);
					}
				});
				builder2.create().show();
				dismiss();
			}
		});
		builder.setTitle("一覧");
		builder.setView(lv);
		builder.setNegativeButton("cancel", null);
		Log.d("posip",String.valueOf(position));
		return builder.create();
		
	}
	
	
}
