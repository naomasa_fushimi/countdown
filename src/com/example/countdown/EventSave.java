package com.example.countdown;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import android.content.Context;
import android.util.Log;

public class EventSave {
	private static final String FileName = "event_save_file.txt";
	Context context;
	EventSave(Context c){
		context = c;
	}
	
	public void save(String event,String year,String month,String day){
		
		try {
			FileOutputStream fileo = context.openFileOutput(FileName,Context.MODE_APPEND);
			OutputStreamWriter osw = new OutputStreamWriter(fileo);
			BufferedWriter bw = new BufferedWriter(osw);
			bw.write(event+","+year+","+month+","+day+",\r\n");
			Log.d("save",event+","+year+","+month+","+day+",\r\n");
			bw.close();
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
	}
}
