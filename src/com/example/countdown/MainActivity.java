package com.example.countdown;


import java.util.ArrayList;

import android.support.v7.app.ActionBarActivity;
import android.text.SpannableStringBuilder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity implements OnClickListener {

	EditText edit1,edit2,edit3,edit4;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		TextView text1 = (TextView) findViewById(R.id.textView1);
		text1.setText("イベント名");
		TextView text2 = (TextView) findViewById(R.id.textView2);
		text2.setText("年");
		TextView text3 = (TextView) findViewById(R.id.textView3);
		text3.setText("月");
		TextView text4 = (TextView) findViewById(R.id.textView4);
		text4.setText("日");
		
		Button button1 = (Button) findViewById(R.id.button1);
		button1.setText("計算");
		button1.setOnClickListener(this);
		Button button2 = (Button) findViewById(R.id.button2);
		button2.setText("保存");
		button2.setOnClickListener(this);
		Button button3 = (Button) findViewById(R.id.button3);
		button3.setText("一覧");
		button3.setOnClickListener(this);
		Button button4 = (Button) findViewById(R.id.button4);
		button4.setText("更新");
		button4.setOnClickListener(this);
		
		/*
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		*/
	}

	/**
	 * A placeholder fragment containing a simple view.
	 
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}*/
	

	@Override
	public void onClick(View v) {
		TextView text5 = (TextView)findViewById(R.id.textView5);
		edit1 = (EditText)findViewById(R.id.editText1);
		edit2 = (EditText)findViewById(R.id.editText2);
		edit3 = (EditText)findViewById(R.id.editText3);
		edit4 = (EditText)findViewById(R.id.editText4);
		SpannableStringBuilder sb1 = (SpannableStringBuilder)edit1.getText();
		SpannableStringBuilder sb2 = (SpannableStringBuilder)edit2.getText();
		SpannableStringBuilder sb3 = (SpannableStringBuilder)edit3.getText();
		SpannableStringBuilder sb4 = (SpannableStringBuilder)edit4.getText();
		String e = sb1.toString();
		String y = sb2.toString();
		String m = sb3.toString();
		String d = sb4.toString();
		DateCount datecount = new DateCount();
		Log.d("e,y,m,d",e);
		switch(v.getId()){
		case R.id.button1:
			//deleteFile("event_save_file.txt");
			if(!y.equals("") & !m.equals("")& !d.equals("")){
				int year = Integer.valueOf(y);
				int month = Integer.valueOf(m);
				int day = Integer.valueOf(d);
				int count = datecount.main2(year, month, day);
				if(count<0){
					text5.setText("もう過ぎた日付です");
				}
				else{
					text5.setText(e+"まで あと"+count+"日");	
				}
			}
			else{
				text5.setText("空欄があります");
			}
			Log.d("clicklistener","button1 click");
			break;
		case R.id.button2:
			//deleteFile("event_save_file.txt");
			if(!y.equals("") & !m.equals("")&!d.equals("")){
				int year2 = Integer.valueOf(y);
				int month2 = Integer.valueOf(m);
				int day2 = Integer.valueOf(d);
				int count = datecount.main2(year2, month2, day2);
				if(count<0){
					text5.setText("もう過ぎた日付です。保存できません。");
				}
				else{
					EventSave es = new EventSave(this);
					es.save(e,y,m,d);
					text5.setText("保存しました");
				}
			}
			else{
				text5.setText("空欄があります");
			}
			break;
		case R.id.button3:
			EventRead er = new EventRead(this);
			TouchDialog dialog = new TouchDialog();
			dialog.listview(er.readname(),this);
			dialog.show(getFragmentManager(), "");
			break;
		case R.id.button4:
			AutoDelete ad = new AutoDelete(this);
			ad.autodelete();
			break;
		default:
			break;
		}
	}
		public void counterView(int position){
			Log.d("counterView","called");
			EventRead er = new EventRead(this);
			ArrayList<String> list = er.readall();
			DateCount datecount = new DateCount();
			String line = String.valueOf(list.get(position));
			String[] line2 = line.split(",");
			String event = line2[0];
			int yy = Integer.valueOf(line2[1]);
			int mm = Integer.valueOf(line2[2]);
			int dd = Integer.valueOf(line2[3]);
			int cc = datecount.main2(yy, mm, dd);
			if(cc != 0){
				TextView text6 = (TextView)findViewById(R.id.textView6);
				text6.setText(event+"("+line2[1]+"/"+line2[2]+"/"+line2[3]+")"+"まであと");
				TextView text7 = (TextView)findViewById(R.id.textView7);
				text7.setText(cc+"日");
			}
			else{
				TextView text6 = (TextView)findViewById(R.id.textView6);
				text6.setText("今日は");
				TextView text7 = (TextView)findViewById(R.id.textView7);
				text7.setText(event+"当日です！");				
			}
		}
}


