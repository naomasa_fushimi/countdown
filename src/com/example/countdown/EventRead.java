package com.example.countdown;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.content.Context;


public class EventRead {
	private static final String FileName = "event_save_file.txt";
	ArrayList<String> list = new ArrayList<String>();
	
	Context context;
	EventRead(Context c){
		context=c;
	}
	
	ArrayList<String> readall(){
		try {
			FileInputStream filei = context.openFileInput(FileName);
			InputStreamReader isr = new InputStreamReader(filei);
			BufferedReader br = new BufferedReader(isr);
			String line;
			while((line=br.readLine()) !=null){
				list.add(line);
			}
			br.close();
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
		return list;
	}
	
	ArrayList<String> readname(){
		try {
			FileInputStream filei = context.openFileInput(FileName);
			InputStreamReader isr = new InputStreamReader(filei);
			BufferedReader br = new BufferedReader(isr);
			String line;
			while((line=br.readLine()) !=null){
				String[] line2 = line.split(",");
				list.add(line2[0]);
			}
			br.close();
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
		return list;
	}
}
